FROM python:alpine

#COPY all
COPY . /
#SET default WORKDIR to the app folder
WORKDIR /app/
#RUN dependencies 
RUN pip install \
-r ../requirements.txt
#Use port 5000 for the server
EXPOSE 5000
#Point env to app.py
ENV FLASK_APP=app.py
#SET and run flask app and run pytest
CMD flask run
